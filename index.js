console.log("Hello s20");

/*[section] While loop*
	-A while loop takes in an expression/condition
	-Expression are any unit of codes that can evaluated to a value
	-If the condition evaluates to true, the statements inside the block will be */

/*======================*/
// let count = 5;

// while(count !== 0;){
// 	console.log("count: " + count);
	// count = 0; /*infinit loop*/
// }
/*=================*/


let count = 5;

// count = 4
while(count !== 0){ /*while only executes if the condition is true*/
	console.log("count: " + count);
	count--; /*decrements count
	count value // count = count - 1;   */
}


/*=====================================*/
/* [SECTION] Do while */

// instead of "parseInt" we can use 'Number' function make sure capitalize 1st letter
// let number = Number(prompt("Give me a number"));

// do { /*if the condition is not true loop will execute only once*/
// 	console.log("Number: "+ number);
// 	number += 1; /*increments value into 1 */

// } while(number <10); /*number must be less than 10  */

/*Do loop execute 1st before the evalaution
While loop evaluate 1st before execution*/

// let n = 15;

// while(number < 10){
// 	console.log("Number: "+ number);
// 	number +=1;
// }

/*=============================*/

/*[Section ] For loop */
/*       initialize // condition // change of value*/
for(let count = 10; count<=20; count++){
	console.log(count)
}

let myString = "tupe"; /*t = index 0, u = index 1, p = index 2, e = index 3*/
console.log("myString length: "+ myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);


for(i = 0; i < myString.length; i++){
	console.log(myString[i]);
}


let myName = "AlEx";
let vowelCount = 0;
for(let i=0; i<myName.length; i++){
    if(
        myName[i].toLowerCase() == "a" ||
        myName[i].toLowerCase() == "e" ||
        myName[i].toLowerCase() == "i" ||
        myName[i].toLowerCase() == "o" ||
        myName[i].toLowerCase() == "u" 
    ){
        console.log(3);
        vowelCount ++;
    }
    else{
        console.log(myName[i]);
    }
}

console.log("Vowels: " + vowelCount);
